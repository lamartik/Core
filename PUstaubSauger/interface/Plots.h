#include <memory>
#include <vector>
#include <algorithm>

#include <TH2F.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"

namespace DAS::PUstaub {

static const std::vector<double> pthat_edges = {0, 1e-6, 15, 30, 50, 80, 120, 170, 300, 470, 600, 800, 1000, 1400, 1800, 2400, 3200, 6500};
static const int nPtHatBins = pthat_edges.size()-1;

struct Plots {

    TString name;

    std::unique_ptr<TH1F> pthatME, pthatPU, pthatMax;
    std::unique_ptr<TH2F> pthatLogw, genptLogw, genmjjLogw, recptLogw, recmjjLogw;

    Plots (const char * Name) :
        name(Name),
        pthatME(std::make_unique<TH1F>("pthatME", ";#hat{p}_{T}^{ME}   [GeV];N_{eff}^{collisions}", nPtHatBins, pthat_edges.data())),
        pthatPU(std::make_unique<TH1F>("pthatPU", ";#hat{p}_{T}^{PU}   [GeV];N_{eff}^{collisions}", nPtHatBins, pthat_edges.data())),
        pthatMax(std::make_unique<TH1F>("pthatMax", ";max #hat{p}_{T}^{ME+PU}   [GeV];N_{eff}^{bx}", nPtHatBins, pthat_edges.data())),
        pthatLogw(std::make_unique<TH2F>("pthatLogw", ";#hat{p}_{T}   [GeV];log(w);N_{eff}^{j}", nPtHatBins, pthat_edges.data(), 400, -30, 20)),
        genptLogw(std::make_unique<TH2F>("genptLogw", ";Jet p_{T}^{gen}   [GeV];log(w);N_{eff}^{j}", nPtBins, pt_edges.data(), 400, -30, 20)),
        genmjjLogw(std::make_unique<TH2F>("genmjjLogw", ";m_{jj}^{gen}   [GeV];log(w);N_{eff}^{jj}", nMjjBins, Mjj_edges.data(), 400, -30, 20)),
        recptLogw(std::make_unique<TH2F>("recptLogw", ";Jet p_{T}^{rec}   [GeV];log(w);N_{eff}^{j}", nPtBins, pt_edges.data(), 400, -30, 20)),
        recmjjLogw(std::make_unique<TH2F>("recmjjLogw", ";m_{jj}^{rec}   [GeV];log(w);N_{eff}^{jj}", nMjjBins, Mjj_edges.data(), 400, -30, 20))
    { }

private:

    float FillPtHat (const Event& event, const PileUp& pileup)
    {
        float gW = event.genWgts.front();

        pthatME->Fill(event.hard_scale, gW);
        float maxpthat = event.hard_scale;
        for (auto pthat: pileup.pthats) {
            pthatPU->Fill(pthat, gW);
            maxpthat = std::max(maxpthat, pthat);
        }
        pthatMax->Fill(maxpthat, gW);

        return maxpthat;
    }

    template<typename Jet> void FillObsWgt (std::unique_ptr<TH2F>& ptLogw,
                                            std::unique_ptr<TH2F>& mjjLogw,
                                            const std::vector<Jet>& jets, float evW)
    {
        for (const auto& jet: jets) {
            auto jW = log(jet.weights.front());
            ptLogw->Fill(jet.p4.Pt(), evW + jW);
        }

        if (jets.size() < 2) return;
        const auto& j0 = jets.at(0),
                    j1 = jets.at(1);                                     

        if (j0.p4.Pt() < 100. || j1.p4.Pt() < 50.) return;
        if (std::max(j0.AbsRap(), j1.AbsRap()) >= 3.0) return;
        auto dijet = j0 + j1;
        auto djW = log(j0.weights.front()) + log(j1.weights.front());
        mjjLogw->Fill(dijet.M(), evW + djW);
    }

public:

    void operator() (const std::vector<GenJet>& genjets,
                     const std::vector<RecJet>& recjets,
                     const Event& event, const PileUp& pileup)
    {
        float maxpthat = FillPtHat(event, pileup);

        float logw = log(event.genWgts.front());
        pthatLogw->Fill(maxpthat, logw);
        FillObsWgt<GenJet>(genptLogw, genmjjLogw, genjets, logw);

        logw += log(event.recWgts.front());
        FillObsWgt<RecJet>(recptLogw, recmjjLogw, recjets, logw);
    }

    void Write (TDirectory * d)
    {
        d->cd();
        TDirectory * dd = d->mkdir(name);
        dd->cd();
        vector<TH1*> hists {pthatME.get(), pthatPU.get(), pthatMax.get(),
                            dynamic_cast<TH1*>(pthatLogw.get()),
                            dynamic_cast<TH1*>(genptLogw.get()),
                            dynamic_cast<TH1*>(genmjjLogw.get()),
                            dynamic_cast<TH1*>(recptLogw.get()),
                            dynamic_cast<TH1*>(recmjjLogw.get())};
        for (auto h: hists) {
            h->SetDirectory(dd);
            TString hname = h->GetName();
            hname.ReplaceAll(name, "");
            h->Write(hname);
        }
    }
};

}
