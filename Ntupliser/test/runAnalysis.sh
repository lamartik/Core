#!/bin/zsh
set -e

folder="$1"
input_file="$(readlink -f "$2")"
isMC="$3"

mkdir -p "$folder"
cd "$folder"

# dummy config files
echo "40 74" > triggers.txt

# diff $CMSSW_BASE/python/Core/Ntupliser/Ntupliser_cfg.py $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py (TODO)
cmsRun $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py inputFiles=file:"$input_file" outputFile=ntuple.root configFile=$CMSSW_BASE/src/Core/Ntupliser/test/CI.json
mergeNtuples ntuple.root mergeNtuples.root "$isMC" 4 2016 /dev/null /dev/null false -f

# MET
applyMETfilters mergeNtuples.root applyMETfilters.root
mergeNtuples ntuple.root mergeNtuples.root "$isMC" 4 2016 /dev/null /dev/null true -f
# TODO getMETfraction

# jet veto maps
wget -nc https://github.com/cms-jet/JECDatabase/raw/master/jet_veto_maps/Summer19UL16_V0/hotjets-UL16.root
getConservativeMap hotjets-UL16.root eff_map_UL16.root h2hot_ul16
rm hotjets-UL16.root
applyConservativeVetoMap mergeNtuples.root applyConservativeVetoMap.root eff_map_UL16.root
mergeNtuples ntuple.root mergeNtuples.root "$isMC" 4 2016 /dev/null eff_map_UL16.root true -f

previous_file=mergeNtuples.root

if [ "$isMC" = "yes" ]; then
    # normalisation
    getSumWeights mergeNtuples.root getSumWeights.root 0
    applyMClumi mergeNtuples.root applyMClumi.root 1 -f

    # PU staub
    applyPUcleaning applyMClumi.root applyPUcleaning.root /dev/null -f

    # jet energy corrections
    getJEStable Summer19UL16_V7_MC
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root Summer19UL16_V7_MC -f

    # jet energy resolution
    getJetResponse applyJEScorrections.root getJetResponse.root
    getJERtable Summer20UL16_JRV3_MC
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root Summer20UL16_JRV3_MC stochasticOnly -f

    # PU profile
    getPUprofile applyJERsmearing.root getPUprofile.root triggers.txt
    applyPUprofCorrection applyJERsmearing.root applyPUprofCorrection.root getPUprofile.root getPUprofile.root 4
    applyDiffPUprofCorrection applyJERsmearing.root applyDiffPUprofCorrection.root getPUprofile.root getPUprofile.root triggers.txt 4 -f

    previous_file=applyDiffPUprofCorrection.root
fi

# MN observables
getMNobservables $previous_file getMNobservables.root

# muons
wget -nc https://gitlab.cern.ch/DasAnalysisSystem/tables/-/raw/master/Rochester/RoccoR2016aUL.txt
applyRochesterCorr $previous_file applyRochesterCorr.root RoccoR2016aUL.txt false -f
wget -nc https://gitlab.cern.ch/DasAnalysisSystem/tables/-/raw/master/Muons/2016_preVFP/Efficiency_muon_generalTracks_Run2016preVFP_UL_trackerMuon.root
applyMuonEffCorr applyRochesterCorr.root applyMuonEffCorr.root Efficiency_muon_generalTracks_Run2016preVFP_UL_trackerMuon.root:NUM_TrackerMuons_DEN_genTracks -f
getDimuonSpectrum applyMuonEffCorr.root getDimuonSpectrum.root
previous_file=applyMuonEffCorr.root

#if [ "$isMC" = "yes" ]; then # TODO
#    wget -nc https://gitlab.cern.ch/DasAnalysisSystem/tables/-/raw/master/triggers/muons/ScaleFactor_DoubleMuonTriggers_UL2018.root
#    applyDimuonTriggerStrategy applyMuonEffCorr.root applyDimuonTriggerStrategy.root ScaleFactor_DoubleMuonTriggers_UL2018.root:ScaleFactorTight_UL2018 -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi

# meta information
getMetaInfo $previous_file meta.info
cat meta.info

# reproducibility
mergeNtuples ntuple.root mergeNtuples.root -c meta.info -f
previous_file=mergeNtuples.root
if [ "$isMC" = "yes" ]; then
    applyMClumi mergeNtuples.root applyMClumi.root -c meta.info -f
    applyPUcleaning applyMClumi.root applyPUcleaning.root -c meta.info -f
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root -c meta.info -f
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root -c meta.info -f
    applyDiffPUprofCorrection applyJERsmearing.root applyDiffPUprofCorrection.root -c meta.info -f
    previous_file=applyDiffPUprofCorrection.root
fi
applyRochesterCorr $previous_file applyRochesterCorr.root -c meta.info -f
applyMuonEffCorr applyRochesterCorr.root applyMuonEffCorr.root -c meta.info -f
previous_file=applyMuonEffCorr.root
#if [ "$isMC" = "yes" ]; then # TODO
#    applyDimuonTriggerStrategy applyMuonEffCorr.root applyDimuonTriggerStrategy.root -c meta.info -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi
getMetaInfo $previous_file meta2.info
diff meta.info meta2.info
