#pragma once

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Mimics a `pair<float,int>`, where the float contains a weight (either gen
/// or rec level, for events, jets, photons, etc.), and the int an index.
/// For bin-to-bin correlated (uncorrelated) uncertainties, it should always be
/// 0 (correspond to the index of the bin containing the corrections). This
/// allows to handle the decorrelations coming from independent stat in different
/// bins.
///
/// A few operators are overloaded so that the weight may be used just as a float.
struct Weight {
    float v = 1; //!< value
    int i = 0; //!< correlation index
    operator float () const { return v; }
    Weight& operator= (const float v) { this->v = v; return *this; }
};

inline float operator* (const Weight& w, const float v) { return w.v * v; }
inline float operator* (const float v, const Weight& w) { return w.v * v; }
inline Weight& operator*= (Weight& w, const float v) { w.v *= v; return w; }
inline Weight& operator/= (Weight& w, const float v) { w.v /= v; return w; }
inline float operator* (const Weight& w1, const Weight& w2) { return w1.v * w2.v; }

}
