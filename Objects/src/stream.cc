#include "Core/Objects/interface/stream.h"

using namespace std;
using namespace DAS;

ostream& operator<< (ostream& Stream, const FourVector& p4)
{
    Stream << '(' << p4.Pt() << ',' << p4.Eta() << ',' << p4.Phi() << ')';
    return Stream;
}

ostream& operator<< (ostream& Stream, const GenMuon& muon)
{
    Stream << muon.p4 << ' ' << muon.Q;
    return Stream;
}
ostream& operator<< (ostream& Stream, const RecMuon& muon)
{
    Stream << muon.p4 << ' ' << muon.Q << ' ' << muon.selectors;
    return Stream;
}

ostream& operator<< (ostream& Stream, const GenJet& jet)
{
    Stream << jet.p4 << ' ' << jet.nCHadrons << ' ' << jet.nBHadrons;
    return Stream;
}
ostream& operator<< (ostream& Stream, const RecJet& jet)
{
    Stream << jet.p4;
    if (jet.nCHadrons >= 0 && jet.nBHadrons >= 0)
        Stream << '\t' << jet.nCHadrons << '\t' << jet.nBHadrons;
    return Stream;
}
