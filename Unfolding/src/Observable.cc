#include "Core/Unfolding/interface/Observable.h"
#include "Core/Unfolding/interface/PtY.h"
#include "Core/Unfolding/interface/MjjYmax.h"
#include "Core/Unfolding/interface/HTn.h"
#include "Core/Unfolding/interface/ZPtY.h"
#include "Core/Unfolding/interface/BF.h"

#include <TH2.h>

#include <boost/property_tree/ptree.hpp>

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

using namespace std;
namespace pt = boost::property_tree;

namespace DAS::Unfolding {

Observable::Observable (const char * name, const char * title) :
    recBinning(name), genBinning(name)
{
    cout << "Defining " << __func__ << " `" << name << "`." << endl;

    recBinning.SetTitle(Form("%s (detector level)", title));
    genBinning.SetTitle(Form("%s (particle level)", title));
}

bool Observable::isMC = false;
float Observable::maxDR = 0.2;

vector<Observable *> GetObservables (boost::property_tree::ptree pt)
{
    vector<Observable *> observables;
#define OBS_TYPES (InclusiveJet::PtY)(DijetMass::MjjYmax)(Rij::HTn)(DrellYan::ZPtY)(ZmmY::BF)(ZmmY::DYJetsToMuMu)(ZmmY::DYJetsToTauTau)(ZmmY::TTTo2L2Nu)(ZmmY::QCD)(ZmmY::WW)(ZmmY::WZ)(ZmmY::ZZ)
#define IF_OBS(r, data, TYPE) \
    if (pt.find(BOOST_PP_STRINGIZE(TYPE)) != pt.not_found()) { \
        observables.push_back(new TYPE); \
        for (auto it = pt.begin(); it != pt.end(); ++it) { \
            if (it->first != BOOST_PP_STRINGIZE(TYPE)) continue; \
            pt.erase(it); \
            break; \
        } \
    }
    BOOST_PP_SEQ_FOR_EACH(IF_OBS, _, OBS_TYPES)
#undef OBS_TYPES
#undef IF_OBS
    if (!pt.empty()) {
        TString unknown;
        for (auto it = pt.begin(); it != pt.end(); ++it) {
            unknown += it->first;
            unknown += ' ';
        }
        BOOST_THROW_EXCEPTION(invalid_argument(Form("Unknown observable(s): %s", unknown.Data())));
    }
    return observables;
}

} // end of namespace DAS::Unfolding
