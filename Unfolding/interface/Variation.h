#ifndef DAS_UNFOLDING_VAR
#define DAS_UNFOLDING_VAR

#include "Core/CommonTools/interface/variables.h" // FourVector
#include "Core/Objects/interface/Weight.h"

#include <list>

#include <TString.h>

#include <MetaInfo.h>

class TH1;
class TH2;
class TDirectory;
class TUnfoldBinning;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Generic structure to hold the histograms and indices corresponding to one
/// variation, for any observable using `TUnfoldBinning`.
///
/// This class is intended to be used in `getUnfHist` only.
struct Variation {

    static bool isMC; //!< flag from metainfo

    static TUnfoldBinning * genBinning, //!< full binning at particle level
                          * recBinning; //!< full binning at detector level

    const TString group;
    const TString name; //!< variation name (including "Up" or "Down")

    const std::size_t index;

    TH1 * rec, //!< reconstructed-level distribution
        * tmp, //!< temporary histogram help fill the covariance matrix
        * gen, //!< generated-level distribution
        * missNoMatch, //!< losses (unmatched entries)
        * missOut, //!< losses (migration out of phase space)
        * fakeNoMatch, //!< background (unmatched entries)
        * fakeOut; //!< background (migration out of phase space)
    // TODO: include miss entries in RM
    // TODO: use TUnfoldBinning to decorrelate categories of miss/fake entries
    TH2 * cov, //!< covariance matrix
        * RM; //!< response matrix

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~Variation ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor
    Variation (const TString& group, const TString& name, size_t index = 0);

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves a correction from a vector. Returns the value at index 0 in
    /// the vector, unless this systematic variation modifies the correction,
    /// in which case the modified value is returned.
    ///
    /// \param group An identifier for the group of variations, used when
    ///              retrieving active variations from the metainfo.
    /// \param corrections A vector of values from which to fetch corrections.
    template<class T>
    const T& getCorrection (const std::string_view& group,
                            const std::vector<T>& corrections) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves the weight of an object in the current variation. This function
    /// uses the static variable `WeightVar` in the object's class to determine
    /// which group it should use.
    ///
    /// \warning The class used to determine the group is determined at compile
    ///          time from the type of the variable passed as argument.
    template<class T>
    const DAS::Weight& getWeight (const T& object) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves the corrected four-momentum of an object in the current variation.
    /// This function uses the static variable `ScaleVar` in the object's class
    /// to determine which group it should use.
    ///
    /// \warning The class used to determine the group is determined at compile
    ///          time from the type of the variable passed as argument.
    template<class T>
    FourVector getCorrP4 (const T& object) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Create a subdirectory to the directory given in argument where all
    /// histograms are written.
    void Write (TDirectory * d);
};

template<class T>
inline const T& Variation::getCorrection (const std::string_view& group,
                                          const std::vector<T>& corrections) const
{
    if (index >= corrections.size() || group != this->group)
        return corrections.front();
    return corrections.at(index);
}

template<class T>
inline const DAS::Weight& Variation::getWeight (const T& object) const
{
    return getCorrection(T::WeightVar, object.weights);
}

template<class T>
inline FourVector Variation::getCorrP4 (const T& object) const
{
    return group == T::ScaleVar ? object.CorrP4(index) : object.CorrP4(0);
}


////////////////////////////////////////////////////////////////////////////////
/// Fill covariance matrix for a given variation (and its `tmp` histogram).
/// The list allows to directly access the non-trivial elements of the histogram.
void fillCov (const Variation&, const std::list<int>&); // TODO: make this function a static member of Observable

////////////////////////////////////////////////////////////////////////////////
/// Get all variations availables according to metainfo.
std::vector<Variation> GetVariations (Darwin::Tools::MetaInfo&);

} // end of namespace DAS::Unfolding

#endif
