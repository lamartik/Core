#ifndef H_MYTUNFOLDDENSITY
#define H_MYTUNFOLDDENSITY

#include <TH2.h>
#include <TUnfoldDensity.h>

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Trick to access protected members of `TUnfoldDensity` or to modify the 
/// behaviour of certain methods, e.g. background subtraction.
class MyTUnfoldDensity : public TUnfoldDensity {

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Get the probability matrix
    /// [`fA`](https://root.cern.ch/doc/master/classTUnfold.html#afa54c390f8178091837e96511c0b5786)
    /// from `TUnfold(Density)` (a priori protected)
    TH2 * GetA (const char * name)
    {
        auto h = new TH2D(name, "probability matrix", fA->GetNrows(), 0.5, 0.5+fA->GetNrows() ,
                                                      fA->GetNcols(), 0.5, 0.5+fA->GetNcols());
        for (int i = 0; i < fA->GetNrows(); ++i)
        for (int j = 0; j < fA->GetNcols(); ++j) {
            double content = (*fA)[i][j];
            h->SetBinContent(i+1,j+1,content);
        }
        return h;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Variadic template to access `TUnfoldDensity::AddRegularisationCondition`
    template<class ...Args> Bool_t MyAddRegularisationCondition (Args ...args)
    {
        return AddRegularisationCondition(args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Variadic template to access the constructor `TUnfoldDensity::TUnfoldDensity`
    template<class ...Args> MyTUnfoldDensity (Args ...args) :
        TUnfoldDensity(args...)
    {}
};

}
#endif
