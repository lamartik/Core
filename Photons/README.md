# Photons

Photon identification uses the centrally provided IDs documented in the
[Egamma TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedPhotonIdentificationRun2).
A correction (scale factor) is needed for the MC to match the efficiency
measured in data. This uses scale factors coming from the
[TWiki page for UltraLegacy](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018).

The photon energy scale and its variations come directly from the ntuples and
no further correction is necessary at analysis level.
