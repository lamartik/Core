#include <iostream>
#include <filesystem>
#include <utility>

#include <TChain.h>
#include <TFile.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"

#include "Core/CommonTools/interface/toolbox.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muons {

////////////////////////////////////////////////////////////////////////////////
/// Checks whether an event should be kept
template<class Muon>
bool keepEvent(const vector<Muon>& muons, float pt1, float pt2)
{
    return muons.size() >= 2 && muons[0].p4.Pt() > pt1 && muons[1].p4.Pt() > pt2;
}

////////////////////////////////////////////////////////////////////////////////
/// Skims the input tree to keep only events with at least two muons, with
/// configurable pT thresholds.
void applyDimuonSkim
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    const bool isMC = metainfo.Get<bool>("flags", "isMC");

    const auto pt1 = config.get<float>("skims.dimuon.pt1");
    metainfo.Set<float>("skims", "dimuon", "pt1", pt1);

    const auto pt2 = config.get<float>("skims.dimuon.pt2");
    metainfo.Set<float>("skims", "dimuon", "pt2", pt2);

    cout << "Skimming events: pt(mu1) > " << pt1 << "\t pt(mu2) > " << pt2 << endl;

    // event information
    Event * event = nullptr;
    tIn->SetBranchAddress("event", &event);

    // muon branches
    vector<RecMuon> * recMuons = nullptr;
    if (branchExists(tIn, "recMuons"))
        tIn->SetBranchAddress("recMuons", &recMuons);

    vector<GenMuon> * genMuons = nullptr;
    if (branchExists(tIn, "genMuons"))
        tIn->SetBranchAddress("genMuons", &genMuons);

    if (!recMuons && !genMuons) {
        BOOST_THROW_EXCEPTION( DE::BadInput("No muons in input tree", tIn) );
    }

    // histogram used in MC for normalisation
    TH1 * hSumWgt = nullptr;
    if (isMC) hSumWgt = new TH1F("hSumWgt",";;#sum_{i} w_{i}", 1, -0.5, 0.5);

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        bool passesRec = !recMuons || keepEvent(*recMuons, pt1, pt2);
        bool passesGen = !genMuons || keepEvent(*genMuons, pt1, pt2);

        if ((steering & DT::fill) == DT::fill && (passesRec || passesGen)) tOut->Fill();

        if (!isMC) continue;
        auto w = event->genWgts.front();
        hSumWgt->Fill(0.0, w);
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    if (hSumWgt) hSumWgt->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Muon namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Selects events with at least two muons passing pT cuts",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("pt1", "skims.dimuon.pt1", "Minimum pT of the first muon")
               .arg<float>("pt2", "skims.dimuon.pt2", "Minimum pT of the second muon");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muons::applyDimuonSkim(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
